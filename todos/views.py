from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

def show_todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos_list": todos,
    }
    return render(request, "todos/list.html", context)



def show_todo_item(request, id):
    todoitems = get_object_or_404(TodoList, id=id)
    context = {
        "todos_item": todoitems,
    }
    return render(request, "todos/detail.html", context)



def create_todo_item(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail",id=item.id)
    else:
        form = TodoListForm()

    context = {
        "todos_object": form,
    }
    return render(request, "todos/create.html", context)
